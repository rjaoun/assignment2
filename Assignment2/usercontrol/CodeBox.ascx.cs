﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment2.usercontrol
{
    public partial class CodeBox : System.Web.UI.UserControl
    {
        public string ChosenCode
        {
            get { return (string)ViewState["CodeIs"];}
            set { ViewState["CodeIs"]=value;}
        }

        DataView CreateCodeSource()
        {
            DataTable codedata = new DataTable();

            //First column is the line number of the code
            DataColumn idx_col = new DataColumn();

            //Second column is the actual code
            DataColumn code_col = new DataColumn();

            DataRow coderow;

            idx_col.ColumnName = "Line";
            idx_col.DataType = System.Type.GetType("System.Int32");

            codedata.Columns.Add(idx_col);

            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            codedata.Columns.Add(code_col);

            List<string> thecode;
            if (ChosenCode == "Teacher_Digital_Design")
            {
                thecode = new List<string>(new string[]
                {
                    "<pre id=\"page-wrapper\">",
                    "~~<p>",
                    "~~~~All the contents of the website will be centerized",
                    "~~~~By wrapping the div with an ID=\"page-wrapper\"",
                    "~~~~and adding css elements to it by making the",
                    "~~~~margin: 0 auto;",
                    "~~~~width: 50%;",
                    "~~<p>",
                    "</pre>"
                });
            }
            else if(ChosenCode =="Own_Digital_Design")
            {
                thecode = new List<string>(new string[]
                {
                    "<pre>",
                    "~~<marquee direction = \"down\" scrolldelay=\"500\">Text here will move. that is so cool",
                    "~~</marquee>",
                    "</pre>"
                });
            }
            else if(ChosenCode == "Teacher_JavaScript")
            {
                thecode = new List<string>(new string[]
                {
                    "~<script>",
                    "~~Document.write(\"Welcome, you have javascript on\")",
                    "~</script>",
                    "~<noscript>",
                    "~~noscript JavaScript is off. Please enable to view full site.",
                    "~</noscript>"
                });
            }
            else if(ChosenCode == "Own_JavaScript")
            {
                thecode = new List<string>(new string[]
                {
                    "<pre>",
                    "~<script>",
                    "~~var myArray =  [\"Rabih\",\"JavaScript\",23, true,\"C#.NET\"]",
                    "~~for (var i = 0; i < 5; i++) {",
                    "~~~console.log(\"value \"+ i + \" is : \"+ myArray[i]);",
                    "~~}",
                    "~~console.log(myArray);",
                    "~~myArray.length = 0;",
                    "~~console.log(myArray);",
                    "~</script>",
                    "</pre>"
                });
            }
            else if(ChosenCode =="Teacher_DataBase")
            {
                thecode = new List<string>(new string[]
                {
                    "--assign table names with CAPS instead of lower case letter",
                    "--to make it easier to read",
                    "SELECT VENDOR_NAME, COUNT(*) AS INVOICE_COUNT,",
                    "SUM(INVOICE_TOTAL) AS INVOICE_TOTAL_SUM",
                    "FROM VENDORS V",
                    "~~JOIN INVOICES I",
                    "~~~~ON V.VENDOR_ID = I.VENDOR_ID",
                    "~~~GROUP BY VENDOR_NAME",
                    "~~~ORDER BY INVOICE_COUNT DESC"
                });
            }
            else if(ChosenCode == "Own_DataBase")
            {
                thecode = new List<string>(new string[]
                {
                    "--This trick is using modulus to get",
                    "--all the even IDs in the table.",
                    "SELECT INVOICE_ID",
                    "FROM INVOICES",
                    "~WHERE MOD(INVOICE_ID,2) = 0"
                });
            }
            else
            {
                thecode = new List<string>();
            }

            int i = 0;
            foreach (string code_line in thecode)
            {
                coderow = codedata.NewRow();
                coderow[idx_col.ColumnName] = i;
                string formatted_code = System.Net.WebUtility.HtmlEncode(code_line);
                formatted_code = formatted_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = formatted_code;

                i++;
                codedata.Rows.Add(coderow);
            }

            DataView codeview = new DataView(codedata);
            return codeview;
        }


            protected void Page_Load(object sender, EventArgs e)
        {
            // this is for the first code which has ID Code_JavaScript in the DataGrid
            DataView ds = CreateCodeSource();
            Code_Format.DataSource = ds;

            Code_Format.DataBind();

        }
    }
}
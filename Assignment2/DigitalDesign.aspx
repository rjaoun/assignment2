﻿<%@ Page Language="C#" Title="Digital Design" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="DigitalDesign.aspx.cs" Inherits="Assignment2.DigitalDesign" %>

<asp:Content ID="maincss" ContentPlaceHolderID="maincss" runat="server">
    <link rel="stylesheet" href="/css/stylesheet.css" />
</asp:Content>

<asp:Content ID="BodyContent" ContentPlaceHolderID="FirstContent" runat="server">
        <h2>Making website Centered in the middle</h2>
        <p>
            In digital design course we learned this method to center all the 
            content of the website by wrapping body content with ID and 
            giving the css values of that ID width of 100% and margin
            zero for the top and bottom and auto for left and right
        </p>

               
        
        <uctrl:code ChosenCode = "Teacher_Digital_Design" runat="server" />
</asp:Content>
<asp:Content ID="SecondBox" ContentPlaceHolderID="SecondContent" runat="server">
        <h2>Live Text!</h2>
        <p>
            An HTML tag called marquee that makes the text move.
            It has different attributes, direction of the scroll which is up, down,
            right, left. scrolldelay which is in milliseconds. 
        </p>
        <pre>
            <marquee direction="down" scrolldelay="500" >Text here will move. That is so cool</marquee>
        </pre>
        
        <uctrl:code ChosenCode ="Own_Digital_Design" runat="server" />
</asp:Content>

<asp:Content ID="ThirdBox" ContentPlaceHolderID="ThirdContent" runat="server">
    <h2>Font styles without using fonts</h2>
    <p>There are HTML tags for font styles       
    </p>
    <caption>
        <a href="https://getbootstrap.com/docs/4.0/content/code/">getbootstap.com</a>
    </caption>
    <pre>
        <code>Code Style</code>
        <kbd>Keyboard Input Style</kbd>
        <samp>Sample Style</samp>
        <var>Variable Style</var>
    </pre>
</asp:Content>
<asp:Content ID="FourthBox" ContentPlaceHolderID="FourthContent" runat="server">
    <h2>Helpful Links</h2>
    <ul>
        <li><a href="https://getbootstrap.com/docs/4.0/content/code/">Font style wihtout using fonts</a></li>
        <li><a href="https://www.tutorialspoint.com/html/html_marquees.htm">marquee tag</a></li>
    </ul>
</asp:Content>
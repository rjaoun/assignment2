﻿<%@ Page Title="Study Guide" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="StudyGuide.aspx.cs" Inherits="Assignment2.StudyGuide" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>

    <div>
        <header><a href="/">Header Goes Here</a></header>
        <nav>Menu Goes Here</nav>
        <section id="MainContent">

        </section>
        <aside id="Sidebar">Sidebar Goes Here</aside>
        <footer>Footer Goes Here</footer>
    </div>
</asp:Content>

﻿<%@ Page Title="Study Guide" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="StudyGuide.aspx.cs" Inherits="Assignment2.StudyGuide" %>

<asp:Content ID="maincss" ContentPlaceHolderID="maincss" runat="server">
    <link rel="stylesheet" href="/css/stylesheet.css" />
</asp:Content>

<asp:Content ContentPlaceHolderID="FirstContent" runat="server">
    
    <div id="body">
        <!--this is a comment -->
        <div class="jumbotron" style="background-image:url('images/background.png')">
            <h1>Rabih Aoun</h1>
            <p class="lead">Junior Web Developer</p>
        </div>                 
    </div>
</asp:Content>

<asp:Content ID="SecondBox" ContentPlaceHolderID="SecondContent" runat="server">
    <div id="SideBar">
        <h2>Contact Me:</h2>
        <address>
            Mississauga<br />
            Ontario<br />
            <abbr title="Phone">P:</abbr>
            (647)111-2222
        </address>

        <address>
            <strong>Support:</strong>   <a href="mailto:Support@example.com">Support@example.com</a><br />
            <strong>Marketing:</strong> <a href="mailto:Marketing@example.com">Marketing@example.com</a>
        </address>
    </div>
</asp:Content>

<asp:Content ID="ThirdBox" ContentPlaceHolderID="ThirdContent" runat="server">
    <h2>About ME</h2>
    <p>
        Web development is a fun hooby for me, I enjoy writing codes, creating websites, changing layouts,
        discovering new methods, learn something new, create as much projects as I could, attend web development 
        meetings. I graduate in 2017 from seneca with computer programmer Diploma, and currently I am studying at Humber College
        to get web development graduate certificate which will help me start my own business. I always thrive to learn more. 
    </p>
</asp:Content>

<asp:Content ID="FourthBox" ContentPlaceHolderID="FourthContent" runat="server">
    <h2>Projects</h2>
    <ul>
        <li>JavaScript</li>
        <li>Digital Design</li>
        <li>Database</li>
        <li>C#.NET</li>
        <li>Project Management</li>
    </ul>
</asp:Content>
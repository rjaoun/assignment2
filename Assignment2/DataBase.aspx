﻿<%@ Page Title="DataBase" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DataBase.aspx.cs" Inherits="Assignment2.DataBase" %>

<asp:Content ID="maincss" ContentPlaceHolderID="maincss" runat="server">
    <link rel="stylesheet" href="/css/stylesheet.css" />
</asp:Content>

<asp:Content ID="FirstBox" ContentPlaceHolderID="FirstContent" runat="server">
    <h2>Renaming tables with Capital letters instead of small letters</h2>
    <p>
        We are joining two tables INNER JOIN and we use 
        Capital V for vendors and Capital I for invoices
        which will make the code easier to read and understand.
    </p>

    <uctrl:code ChosenCode ="Teacher_DataBase" runat="server" />
</asp:Content>

<asp:Content ID="SecondBox" ContentPlaceHolderID="SecondContent" runat="server">
    <h2>Even Numbers Tricks</h2>
    <p>
        This trick I have learned about the modulus and how to use it in SQL.
        we will use an Expression to get all the even IDs in the table. To get all 
        the odd IDs, we just use number 1 instead of zero.
    </p>

    <uctrl:code ChosenCode ="Own_DataBase" runat="server" />
</asp:Content>

<asp:Content ID="ThirdBox" ContentPlaceHolderID="ThirdContent" runat="server">
    <h2>Generating number of rows using dual table</h2>
    <p>
        In this example we will see how we can generate number of rows in order
        in whatever size we want. We chose number 20 to have 20 rows in order.
    </p>
    <caption>
        <a href="https://www.quora.com/What-are-some-cool-SQL-queries-or-tricks-that-you-might-not-think-of-being-able-to-do-with-SQL">Generating number of rows</a>
    </caption>
    <pre>
        select level a from dual connect by level <= 20
    </pre>
</asp:Content>

<asp:Content ID="FourthBox" ContentPlaceHolderID="FourthContent" runat="server">
    <h2>Helpful Links</h2>
    <ul>
        <li><a href="https://simonborer.github.io/db-course/slides/notes/week-2/">Join Tables</a></li>
        <li><a href="https://www.quora.com/What-are-some-cool-SQL-queries-or-tricks-that-you-might-not-think-of-being-able-to-do-with-SQL">generate number of rows</a></li>
    </ul>
</asp:Content>
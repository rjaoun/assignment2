﻿<%@ Page Language="C#" Title="JavaScript" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="javascript.aspx.cs" Inherits="Assignment2.javascript" %>

    <asp:Content ID="maincss" ContentPlaceHolderID="maincss" runat="server">
        <link rel="stylesheet" href="/css/stylesheet.css" />
    </asp:Content>

<asp:Content ID="FirstBox" ContentPlaceHolderID="FirstContent" runat="server">
    <h2>JavaScript ON/OFF</h2>

    <p>
        This tricky code is to show if the browser has JavaScript on/off.
        I have added the following code to show you how easy it is to 
        determine if javascript is on or off.
    </p>
    <script type="text/javascript">
          document.write("Welcome, you have Javascript on.")
    </script>   
    <noscript>JavaScript is off. Please enable to view full site.</noscript>  

    <uctrl:code ChosenCode = "Teacher_JavaScript" runat="server" />
</asp:Content>

<asp:Content ID="SecondBox" ContentPlaceHolderID="SecondContent" runat="server">
        <h2>My Own JavaScript Snippet</h2>
        <p>
            This is a snippet I created it which is very helpful to use.
            if we want to empty an array, instead of removing array 
            items one by one, we could remove them
            all with just one line of code which is setting the length of 
            the array to zero.
        </p>
        <script>
        /******************************************************************
         * This javaScript snippet code I created myself. We will have    *
         * an array assigned with five values and use for loop to display *  
         * this array in console log with the values, then I will use a   * 
         * simple trick to remove all the values from the array with one  *
         * line of code by making the length of an array equal to zero    *
         ******************************************************************/
                var myArray = ["Rabih","JavaScript",23, true,"C#.NET"];
                for (var i = 0; i < 5; i++) {
                    console.log("value "+ i + " is : "+ myArray[i]);
                }
        

                console.log(myArray);
                myArray.length = 0;
                console.log(myArray);
        </script>

        <uctrl:code ChosenCode = "Own_JavaScript" runat="server" />
</asp:Content>

<asp:Content ID="ThirdBox" ContentPlaceHolderID="ThirdContent" runat="server">
        <h2>Cool JavaScript Snippet</h2>
        <!-- I had to leave the first part of javascript shown in the Javascript page 
        to show how the background color changes-->
        <p>
            This javaScript snippet was taken from CSS-TRICKS website.     
            It was edited by CHRIS COYIER                                    
            https://css-tricks.com/snippets/javascript/random-hex-color/    
            It will randomly change the background color of the pre element       
            everytime we refresh the page
        </p>
        <caption>
            <a href="https://css-tricks.com/snippets/javascript/random-hex-color/">Background Color Randomly Change</a>
        </caption>
        <p>This is <strong><span id="colorcode"></span> Color.</strong>
            Reload the page for another color
        </p>
        <p>javascript:</p>
        <pre id="box">var randomColor = Math.floor(Math.random()*16777215).toString(16);</pre>
        <script type="text/javascript">
        /******************************************************************
         * This javaScript snippet was taken from CSS-TRICKS website.     *
         * It was edited by CHRIS COYIER                                  *  
         * https://css-tricks.com/snippets/javascript/random-hex-color/   * 
         * It will randomly change the background color of the pre element*
         * everytime we refresh the page                                  *
         ******************************************************************/
        $(function() {

        var randomColor = Math.floor(Math.random()*16777215).toString(16);

        $("pre").css({
    
            backgroundColor: '#' + randomColor
    
        });
    
        $("#colorcode").text("#" + randomColor);

        });
    </script>
    
</asp:Content>

<asp:Content ID="FourthBox" ContentPlaceHolderID="FourthContent" runat="server">
    <h2>Helpful Links</h2>
    <ul>
        <li><a href="https://www.jstips.co/en/javascript/two-ways-to-empty-an-array/">Empty an Array</a></li>
        <li><a href="https://css-tricks.com/snippets/javascript/random-hex-color/">CSS-Tricks</a></li>
    </ul>
</asp:Content>